:sequential_nav: next

..  _tutorial-wannier-SrTiO3:


Preparing interface between SIESTA and Wannier90 (example: :math:`SrTiO_3`)
=============================================================================

In this tutorial, you will learn how to prepare the interface between SIESTA and Wannier90, with the example of :math:`SrTiO_3` in the cubic phase. You can follow the slides by Javier Junquera, which can be downloaded here:

* :download:`Exercise-Wannier-SrTiO3.pdf<work-files/Exercise-Wannier-SrTiO3.pdf>`

You can download the needed file for this tutorial.
   

* :download:`Sr.psf<work-files/Sr.psf>`
* :download:`Ti.psf<work-files/Ti.psf>`
* :download:`O.psf<work-files/O.psf>`
* :download:`SrTiO3.fdf<work-files/SrTiO3.fdf>`
* :download:`SrTiO3.win<work-files/SrTiO3.win>`
