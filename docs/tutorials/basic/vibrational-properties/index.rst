:sequential_nav: next

..  _tutorial-basic-vibrational-properties:

Vibration modes and phonons
===========================

..  sidebar:: **Have you set up the local environment?**

    If not, :ref:`do that now <local_installation>` before proceeding.

.. note::
   For background, see the
   excellent `slide presentation <https://drive.google.com/file/d/1SRhSOWwFVPmS3vr4REJSvqgXf2Tic8fl/view?usp=sharing>`_
   by Andrei Postnikov. 
   
.. note::
   Objectives: Finite-differences to compute force constants.
               Molecules vs crystals. Supercells to represent the
	       real-space FCs. Computations on various systems,
	       including some visualization (with Andrei's tools).
.. note::
   Exercises:  To choose from 'Phonons' legacy tutorials
   directory. There is a lot of material, some relatively new by
   Andrei on a BN monolayer and a Au chain.
   
.. toctree::
    :maxdepth: 1

    Benzene/index
    Si-bulk/index
   

   
    
	       
   
   

   
  
  
   

   
