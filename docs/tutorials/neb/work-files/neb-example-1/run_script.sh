#!/bin/sh
#-----------------------------------
#  run_script for Running Siesta
#  A.Akhtar 23/3/2021
#-----------------------------------
echo 'Flos Lib Loaded'
export LUA_PATH="$HOME/Projects/flos/?.lua;$HOME/Projects/flos/?/init.lua;$LUA_PATH;;"

SIESTA=/home/aakhtar/Projects/siesta-psml/Obj-gfortran-openmpi-lua/siesta
folder_name=results
np=8

#-------------------------------------
if [ -d $folder_name ] ; then
  echo "Directory $foldername exists..."
  exit
fi

mkdir $folder_name
cp *.psf *.psml *.DM* *.XV *.fdf *.lua *.xyz $folder_name
cd $folder_name
mpirun -n $np --use-hwthread-cpus $SIESTA < input.fdf > output.out  &
#---------
#cd ..
